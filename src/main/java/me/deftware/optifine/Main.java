package me.deftware.optifine;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.main.Bootstrap;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.utils.OSUtils;
import me.deftware.client.framework.wrappers.IChat;
import me.deftware.client.framework.wrappers.IMinecraft;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class Main extends EMCMod {

    private static final String dataURL = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/plugins/optifine/versions.json";
    private Logger logger = LogManager.getLogger("EMC OptiFine Installer");
    private static final String TEMP_PATH = OSUtils.getMCDir() + ".temp_optifine_installer" + File.separator;

    @Override
    public void initialize() {
        if (new File(TEMP_PATH).exists()) {
            try {
                FileUtils.deleteDirectory(new File(TEMP_PATH));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        // 477 = Minecraft 1.14
        if (IMinecraft.getMinecraftProtocolVersion() < 477 || FrameworkConstants.MAPPING_SYSTEM != FrameworkConstants.MappingSystem.Yarn) {
            logger.error("The OptiFine installer only works on 1.14+");
            Bootstrap.callMethod("EMC-Marketplace", "setStatus(The OptiFine plugin is only for Minecraft 1.14 and above)", "OptiFine Installer", null);
        } else {
            new Thread(() -> {
                Thread.currentThread().setName("OptiFine Downloader");
                try {
                    String emcPath = OSUtils.getMCDir() + "libraries" + File.separator + "me" + File.separator + "deftware" + File.separator + "EMC-F" + File.separator + getEMCJsonVersion() + File.separator;
                    logger.debug("EMC path is " + emcPath);
                    if (!new File(emcPath + "optifabric.jar").exists() || !new File(OSUtils.getMCDir(false) + "mods" + File.separator + "optifine.jar").exists()) {
                        if (!new File(TEMP_PATH).exists()) {
                            new File(TEMP_PATH).mkdirs();
                        }
                        JsonObject data = new Gson().fromJson(get(dataURL), JsonObject.class).get(IMinecraft.getMinecraftVersion()).getAsJsonObject();

                        if (data.get("available").getAsBoolean()) {
                            File optifineJar = new File(data.get("optifine_path").getAsString().replace("%mc%", OSUtils.getMCDir(false)).replace("%emc%", emcPath));
                            File optifabricJar = new File(data.get("optifabric_path").getAsString().replace("%mc%", OSUtils.getMCDir()).replace("%emc%", emcPath));

                            // Remove Optifabric and Optifine if present to replace
                            if (!optifineJar.getParentFile().exists()) {
                                optifineJar.getParentFile().mkdirs();
                            }
                            if (!optifabricJar.getParentFile().exists()) {
                                optifabricJar.getParentFile().mkdirs();
                            }

                            try {
                                if (optifineJar.exists()) {
                                    if (optifineJar.delete()) {
                                        logger.info("Deleting old OptiFine jar");
                                    }
                                } else {
                                    // Fallback for any other OptiFine Versions installed in the normal mods directory
                                    File[] mcModsData = new File(OSUtils.getMCDir(false) + "mods").listFiles();

                                    if (mcModsData != null) {
                                        for (File dataFile : mcModsData) {
                                            if (dataFile.getName().toLowerCase().contains("optifine")) {
                                                if (dataFile.delete()) {
                                                    logger.info("Deleting old OptiFine jar: " + dataFile.getName());
                                                }
                                            }
                                        }
                                    }
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                            Bootstrap.callMethod("EMC-Marketplace", "setStatus(Installing OptiFine, do not close Minecraft...)", "OptiFine Installer", null);
                            IChat.sendClientMessage("Installing OptiFine for " + IMinecraft.getMinecraftVersion());

                            logger.info("Installing OptiFine for EMC " + getEMCVersion() + "...");
                            installOptifine(data.get("version").getAsString(), new File(TEMP_PATH + "optifine.jar"));

                            logger.info("Installed OptiFine, installing OptiFabric...");
                            download(data.get("optifabric").getAsString(), TEMP_PATH + "optifabric.jar");

                            // Copy from temp path to real path
                            logger.info("Installed OptiFabric, copying files from temp path...");
                            FileUtils.copyFile(new File(TEMP_PATH + "optifine.jar"), optifineJar);
                            FileUtils.copyFile(new File(TEMP_PATH + "optifabric.jar"), optifabricJar);

                            logger.info("Installed OptiFine, restart mc");
                            IChat.sendClientMessage("Installed OptiFine, please restart Minecraft");
                            Bootstrap.callMethod("EMC-Marketplace", "setStatus(Installed OptiFine, please restart mc)", "OptiFine Installer", null);
                        } else {
                            logger.warn("OptiFine not available for " + IMinecraft.getMinecraftVersion());
                            Bootstrap.callMethod("EMC-Marketplace", "setStatus(OptiFine is not available for mc " + IMinecraft.getMinecraftVersion() + ")", "OptiFine Installer", null);
                        }
                    } else {
                        Bootstrap.callMethod("EMC-Marketplace", "setStatus(Latest version of OptiFine already installed)", "OptiFine Installer", null);
                    }
                } catch (Exception ex) {
                    IChat.sendClientMessage("Error: Failed to install OptiFine, see latest.log");
                    logger.error("Failed to install OptiFine:");
                    Bootstrap.callMethod("EMC-Marketplace", "setStatus(Could not install OptiFine, see latest.log)", "OptiFine Installer", null);
                    ex.printStackTrace();
                }
            }).start();
        }
    }

    @Override
    public void callMethod(String method, String caller, Object object) {
        if (method.equalsIgnoreCase("uninstall()")) {
            logger.info("Uninstalling OptiFine and OptiFabric...");
            try {
                String path = OSUtils.getMCDir() + "libraries" + File.separator + "me" + File.separator + "deftware" + File.separator + "EMC-F" + File.separator + getEMCVersion() + File.separator;
                FileUtils.writeStringToFile(new File(path + "optifabric.jar.delete"), "Delete mod", "UTF-8");
                File optifine = new File(path + "optifine.jar");
                if (optifine.exists()) {
                    FileUtils.writeStringToFile(optifine, "Delete mod", "UTF-8");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void installOptifine(String optifineVersion, File path) throws Exception {
        String website = get("https://optifine.net/adloadx?f=" + optifineVersion + ".jar");
        website = "https://optifine.net/downloadx" + website.split("downloadx")[1].split("'")[0];
        download(website, path.getAbsolutePath());
    }

    private void download(String uri, String fileName) throws Exception {
        URL url = new URL(uri);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
        connection.setRequestMethod("GET");
        FileOutputStream out = new FileOutputStream(fileName);
        InputStream in = connection.getInputStream();
        int read;
        byte[] buffer = new byte[4096];
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        out.close();
    }

    private String get(String url) throws Exception {
        URL url1 = new URL(url);
        Object connection = (url.startsWith("https://") ? (HttpsURLConnection) url1.openConnection()
                : (HttpURLConnection) url1.openConnection());
        ((URLConnection) connection).setConnectTimeout(8 * 1000);
        ((URLConnection) connection).setRequestProperty("User-Agent", "EMC Installer");
        ((HttpURLConnection) connection).setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(((URLConnection) connection).getInputStream()));
        StringBuilder result = new StringBuilder();
        String text;
        while ((text = in.readLine()) != null) {
            result.append(text);
        }
        in.close();
        return result.toString();
    }

    private String getEMCJsonVersion() throws Exception {
        File jsonFile = getEMCJsonFile();
        String version = null;
        if (!jsonFile.exists()) {
            throw new Exception("Could not find EMC json file!");
        } else {
            for (JsonElement jsonElement : lookupElementInJson(jsonFile, "libraries").getAsJsonArray()) {
                JsonObject entry = jsonElement.getAsJsonObject();
                if (entry.get("name").getAsString().contains("me.deftware:EMC-F")) {
                    version = entry.get("name").getAsString().split(":")[2];
                }
            }
        }
        return version;
    }

}
